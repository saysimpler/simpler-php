
# Simpler PHP 

The Simpler PHP library provides a set of classes for interacting with the Simpler API.

## Requirements

PHP >= 7.1.0 

## Composer

You can install the library via [Composer](http://getcomposer.org/).

```bash
composer require simpler/simpler-php
```

